package dev.luizhalf.teste.api.repositories;

import dev.luizhalf.teste.api.domains.Agente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgenteRepository extends JpaRepository<Agente, Integer> {
}
