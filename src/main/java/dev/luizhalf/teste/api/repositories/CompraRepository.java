package dev.luizhalf.teste.api.repositories;

import dev.luizhalf.teste.api.domains.Compra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompraRepository extends JpaRepository<Compra, Integer> {
}
