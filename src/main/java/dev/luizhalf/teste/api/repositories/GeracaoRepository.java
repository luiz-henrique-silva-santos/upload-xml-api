package dev.luizhalf.teste.api.repositories;

import dev.luizhalf.teste.api.domains.Geracao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeracaoRepository extends JpaRepository<Geracao, Integer> {
}
