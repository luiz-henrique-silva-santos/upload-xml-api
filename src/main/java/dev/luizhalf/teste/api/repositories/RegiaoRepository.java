package dev.luizhalf.teste.api.repositories;

import dev.luizhalf.teste.api.domains.Regiao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegiaoRepository extends JpaRepository<Regiao, Integer> {

    List<Regiao> findAllBySigla(String sigla);

}
