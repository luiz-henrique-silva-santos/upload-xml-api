package dev.luizhalf.teste.api.controllers;

import dev.luizhalf.teste.api.dtos.response.RegiaoResponseDTO;
import dev.luizhalf.teste.api.servies.RegiaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/teste-api/regiao")
public class RegiaoController {

    @Autowired
    RegiaoService regiaoService;

    @GetMapping("/report")
    public ResponseEntity<ArrayList<RegiaoResponseDTO>> generateReport() {
        return ResponseEntity.ok().body(regiaoService.generateReport());
    }

    @GetMapping("/{sigla}/report")
    public ResponseEntity<ArrayList<RegiaoResponseDTO>> generateReportBySigla(@PathVariable String sigla) {
        return ResponseEntity.ok().body(regiaoService.generateReportBySigla(sigla));
    }
}
