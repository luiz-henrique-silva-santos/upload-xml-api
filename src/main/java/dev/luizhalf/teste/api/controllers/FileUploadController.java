package dev.luizhalf.teste.api.controllers;

import dev.luizhalf.teste.api.dtos.response.FileUploadResponseDTO;
import dev.luizhalf.teste.api.servies.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/teste-api/upload")
public class FileUploadController {

    @Autowired
    FileUploadService fileUploadService;

    @PostMapping
    @CrossOrigin
    public ResponseEntity<FileUploadResponseDTO> handleFileUpload(@RequestParam("file") MultipartFile file) {
        FileUploadResponseDTO response = fileUploadService.readFile(file);
        if (response != null) {
            return ResponseEntity.ok().body(response);
        } else {
            return ResponseEntity.internalServerError().build();
        }
    }
}
