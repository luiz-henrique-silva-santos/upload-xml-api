package dev.luizhalf.teste.api.servies;

import dev.luizhalf.teste.api.domains.Agente;
import dev.luizhalf.teste.api.domains.Compra;
import dev.luizhalf.teste.api.domains.Geracao;
import dev.luizhalf.teste.api.domains.Regiao;
import dev.luizhalf.teste.api.dtos.request.AgentesRequestDTO;
import dev.luizhalf.teste.api.dtos.response.FileUploadResponseDTO;
import dev.luizhalf.teste.api.dtos.response.RegiaoResponseDTO;
import dev.luizhalf.teste.api.repositories.AgenteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileUploadService {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadService.class);

    @Autowired
    AgenteRepository agenteRepository;

    public FileUploadResponseDTO readFile(MultipartFile file) {
        try {
            logger.info("Starting read file");
            JAXBContext jaxbContext = JAXBContext.newInstance(AgentesRequestDTO.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AgentesRequestDTO xmlObject = (AgentesRequestDTO) jaxbUnmarshaller.unmarshal(file.getInputStream());
            return saveFile(xmlObject);
        } catch (Exception ex) {
            logger.error("Fail to read file",ex.getMessage());
        }
        return null;
    }

    public FileUploadResponseDTO saveFile(AgentesRequestDTO obj) {

        List<Regiao> regiaoList = new ArrayList<Regiao>();

        obj.getAgente().getRegiao().forEach(regiao -> {

            List<Compra> compraList = new ArrayList<Compra>();
            List<Geracao> geracaoList = new ArrayList<Geracao>();

            regiao.getGeracao().getValor().forEach(valor -> {
                geracaoList.add(Geracao.builder()
                        .valor(Double.parseDouble(valor))
                        .build());
            });

            regiao.getCompra().getValor().forEach(valor -> {
                compraList.add(Compra.builder()
                        .valor(Double.parseDouble(valor))
                        .build());
            });

            regiaoList.add(Regiao.builder()
                    .sigla(regiao.getSigla())
                    .compra(compraList)
                    .geracao(geracaoList)
                    .build());
        });

        Agente agenteToSave = agenteRepository.save(Agente.builder()
                .data(LocalDateTime.parse(obj.getAgente().getData(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssz")))
                .codigo(obj.getAgente().getCodigo())
                .regiao(regiaoList)
                .build());

        ArrayList<RegiaoResponseDTO> regiaoResponseList = new ArrayList<>();
        agenteToSave.getRegiao().forEach(regiao -> {
            regiaoResponseList.add(RegiaoResponseDTO.builder()
                    .sigla(regiao.getSigla())
                    .qtdCompra(String.valueOf(regiao.getCompra().size()))
                    .listaValoresGeracao(regiao.getGeracao().stream().map(reg -> reg.getValor().toString()).collect(Collectors.toList()))
                    .qtdGeracao(String.valueOf(regiao.getGeracao().size()))
                    .listaValoresCompra(regiao.getCompra().stream().map(comp -> comp.getValor().toString()).collect(Collectors.toList()))
                    .build());
        });

        logger.info("Code Agente saved: {}",agenteToSave.getCodigo());

        return FileUploadResponseDTO.builder()
                .idAgente(agenteToSave.getCodigo())
                .regioes(regiaoResponseList)
                .build();
    }
}
