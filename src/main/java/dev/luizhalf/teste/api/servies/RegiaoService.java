package dev.luizhalf.teste.api.servies;

import dev.luizhalf.teste.api.domains.Regiao;
import dev.luizhalf.teste.api.dtos.response.RegiaoResponseDTO;
import dev.luizhalf.teste.api.repositories.RegiaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RegiaoService {

    @Autowired
    RegiaoRepository regiaoRepository;


    public ArrayList<RegiaoResponseDTO> generateReport() {
        List<Regiao> listRegiao = regiaoRepository.findAll();
        return populateReport(listRegiao);
    }

    public ArrayList<RegiaoResponseDTO> generateReportBySigla(String sigla) {
        List<Regiao> listRegiao = regiaoRepository.findAllBySigla(sigla);
        return populateReport(listRegiao);
    }

    public ArrayList<RegiaoResponseDTO> populateReport(List<Regiao> listRegiao) {

        ArrayList<RegiaoResponseDTO> listReport = new ArrayList<>();
        if (listRegiao.size() > 0) {

            List<String> listaSigla = listRegiao.stream().map(regiao -> regiao.getSigla()).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());

            listaSigla.forEach(siglas -> {
                List<Regiao> listFiltered = listRegiao.stream().filter(regiao -> regiao.getSigla() == siglas).collect(Collectors.toList());

                List<String> valoresGeracao = new ArrayList<>();
                List<String> valoresCompra = new ArrayList<>();

                listFiltered.forEach(lista -> {
                    valoresGeracao.addAll(lista.getGeracao().stream().map(r -> r.getValor().toString()).collect(Collectors.toList()));
                    valoresCompra.addAll(lista.getCompra().stream().map(r -> r.getValor().toString()).collect(Collectors.toList()));
                });

                listReport.add(RegiaoResponseDTO.builder()
                        .sigla(siglas)
                        .listaValoresGeracao(valoresGeracao)
                        .qtdGeracao(String.valueOf(valoresGeracao.size()))
                        .listaValoresCompra(valoresCompra)
                        .qtdCompra(String.valueOf(valoresCompra.size()))
                        .build());

            });
        }
        return listReport;
    }
}
