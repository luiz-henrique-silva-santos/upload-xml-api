package dev.luizhalf.teste.api.dtos.request;

import lombok.Data;

import java.util.List;

@Data
public class GeracaoRequestDTO {
    private List<String> valor;
}
