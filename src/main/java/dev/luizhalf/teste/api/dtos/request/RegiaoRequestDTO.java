package dev.luizhalf.teste.api.dtos.request;

import lombok.Data;

import javax.xml.bind.annotation.XmlAttribute;

@Data
public class RegiaoRequestDTO {
    private String sigla;
    private GeracaoRequestDTO geracao;
    private CompraRequestDTO compra;

@XmlAttribute
    public String getSigla() {
        return sigla;
    }
}

