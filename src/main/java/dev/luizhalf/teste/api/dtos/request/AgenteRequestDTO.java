package dev.luizhalf.teste.api.dtos.request;

import lombok.Data;

import java.util.List;

@Data
public class AgenteRequestDTO {
    private String codigo;
    private String data;
    private List<RegiaoRequestDTO> regiao;
}
