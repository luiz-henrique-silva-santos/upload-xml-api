package dev.luizhalf.teste.api.dtos.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Data
@Builder
public class FileUploadResponseDTO {
    private String idAgente;
    private ArrayList<RegiaoResponseDTO> regioes;
}

