package dev.luizhalf.teste.api.dtos.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Builder
public class RegiaoResponseDTO {
    private String sigla;
    private List<String> listaValoresGeracao;
    private String qtdGeracao;
    private List<String> listaValoresCompra;
    private String qtdCompra;
}
