package dev.luizhalf.teste.api.dtos.request;

import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="agentes")
@Setter
public class AgentesRequestDTO {
    private AgenteRequestDTO agente;

    @XmlElement
    public AgenteRequestDTO getAgente() {
        return agente;
    }
}



